<?php

namespace Bitkorn\ShopTcpdf;

use Bitkorn\ShopTcpdf\Controller\Ajax\DemoController;
use Bitkorn\ShopTcpdf\Factory\Controller\Ajax\DemoControllerFactory;
use Bitkorn\ShopTcpdf\Factory\Pdf\Delivery\PdfDeliveryFactory;
use Bitkorn\ShopTcpdf\Factory\Pdf\Invoice\PdfInvoiceFactory;
use Bitkorn\ShopTcpdf\Factory\Pdf\Order\PdfOrderFactory;
use Bitkorn\ShopTcpdf\Factory\Pdf\PdfHeaderFooterBrandFactory;
use Bitkorn\ShopTcpdf\Pdf\Delivery\PdfDelivery;
use Bitkorn\ShopTcpdf\Pdf\Invoice\PdfInvoice;
use Bitkorn\ShopTcpdf\Pdf\Order\PdfOrder;
use Bitkorn\ShopTcpdf\Pdf\PdfHeaderFooterBrand;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            /**
             * demos
             */
            'bitkorn_shoptcpdf_headerfooterbrand' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/shoptcpdf-headerfooterbrand',
                    'defaults' => [
                        'controller' => DemoController::class,
                        'action' => 'headerFooterBrand'
                    ],
                ],
            ],
            'bitkorn_shoptcpdf_pdforder' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/shoptcpdf-pdforder[/:uuid]',
                    'constraints' => [
                        'uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults' => [
                        'controller' => DemoController::class,
                        'action' => 'pdfOrder'
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            DemoController::class => DemoControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [
            PdfHeaderFooterBrand::class => PdfHeaderFooterBrandFactory::class,
            // PDFs complete
            PdfOrder::class => PdfOrderFactory::class,
            PdfInvoice::class => PdfInvoiceFactory::class,
            PdfDelivery::class => PdfDeliveryFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helper_config' => [
        'factories' => [],
        'invokables' => [],
        'aliases' => [],
    ],
    'view_manager' => [
        'template_map' => [],
        'template_path_stack' => [],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
                'text_domain' => 'shop_tcpdf'
            ],
        ],
    ],
    'bitkorn_shoptcpdf' => [
        'protect_passwd' => 'testtext',
        'path_img' => __DIR__ . '/../data/img',
        'path_pdf' => __DIR__ . '/../../../../data/files/documents', // das kommt besser aus bitkorn/shop
        'brand_rgb' => [136, 16, 69],
        'image_paths' => [
            'logo' => '/gomolzig_logo.svg',
            'brand' => '/document_brand.svg',
            'logo_simple' => '/gomolzig_logo_minimal_color.svg',
        ],
        'data_author' => 'Gomolzig Aircraft Services GmbH',
        'address_line_origin' => 'Gomolzig GmbH - Postfach 466 - 58317',
        'footer_text_line' => 'lang_key_pdf_footer_agb_line',
        'footer_html' => '<table style="width: 100%;" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td style="background-color: #993366; width: 1px;"></td>
        <td style="width: 24%;">
            <b style="color: black;">Eisenwerkstr. 9 / D-58332 Schwelm</b>
            <br>
            Telefon: +49 (0) 2336-49 03 30
            <br>
            Telefax: +49 (0) 2336-49 03 39
            <br>
            Steuer-Nr.: 5341 341 57061679
            <br>
            VAT-IDNo.: DE 317 931 865
            <br>
            e-mail: info@gomolzig.de
        </td>
        <td style="background-color: #993366; width: 1px;"></td>
        <td style="width: 12%;">
            <b style="color: black;">Geschäftsführer</b>
            <br>
            T. Kruse
        </td>
        <td style="background-color: #993366; width: 1px;"></td>
        <td style="width: 20%;">
            <b style="color: black;">Genehmigungen</b>
            <br>
            <table>
                <tr><td>EASA.21J.274</td><td>LBA.21J.0200</td></tr>
                <tr><td>DE.21G.0410</td><td>LBA.21G.0410</td></tr>
                <tr><td>DE.145.0930</td><td>LBA.145.0930</td></tr>
                <tr><td>DE.CAO.0043</td><td>LBA.MG.0930</td></tr>
            </table>
        </td>
        <td style="background-color: #993366; width: 1px;"></td>
        <td style="width: 20%;">
            <b style="color: black;">Sparkasse Köln Bonn</b>
            <br>
            IBAN:
            <br>
            DE37 3705 0198 1934 2563 79
            <br>
            USD IBAN:
            <br>
            DE29 3705 0198 1934 4152 15
            <br>
            SWIFT-BIC: COLSDE33
        </td>
        <td style="background-color: #993366; width: 1px;"></td>
        <td style="width: 24%;">
            <b style="color: black;">Werftbetrieb Siegerland</b>
            <br>
            Flugplatz Siegerland EDGS
            <br>
            D-57299 Burbach
            <br>
            Telefon +49 (0) 27 36 – 5 09 68 87
            <br>
            Telefax +49 (0) 27 36 – 5 09 68 92
<!--            <br>
            werft.siegerland@gomolzig.de-->
        </td>
    </tr>
</table>',
    ],
];
