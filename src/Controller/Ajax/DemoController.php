<?php

namespace Bitkorn\ShopTcpdf\Controller\Ajax;

use Bitkorn\ShopTcpdf\Pdf\Order\PdfOrder;
use Bitkorn\ShopTcpdf\Pdf\PdfHeaderFooterBrand;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;

class DemoController extends AbstractUserController
{
    /**
     * @var PdfHeaderFooterBrand
     */
    protected $pdfHeaderFooterBrand;

    /**
     * @var PdfOrder
     */
    protected $pdfOrder;

    /**
     * @param PdfHeaderFooterBrand $pdfHeaderFooterBrand
     */
    public function setPdfHeaderFooterBrand(PdfHeaderFooterBrand $pdfHeaderFooterBrand): void
    {
        $this->pdfHeaderFooterBrand = $pdfHeaderFooterBrand;
    }

    /**
     * @param PdfOrder $pdfOrder
     */
    public function setPdfOrder(PdfOrder $pdfOrder): void
    {
        $this->pdfOrder = $pdfOrder;
    }

    /**
     * @return JsonModel
     */
    public function headerFooterBrandAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->pdfHeaderFooterBrand->setUserdataAddressRecipient(['Douglas Adams', 'Sonnenallee 42', '42000 Anhaltern', 'Milchstraße']);
        $this->pdfHeaderFooterBrand->makeBasics();
        $this->pdfHeaderFooterBrand->AddPage('P');
        $this->pdfHeaderFooterBrand->Cell(25, 6, 'Text', 0, 0, 'L', false);
        $this->pdfHeaderFooterBrand->AddPage('P');
        $this->pdfHeaderFooterBrand->Cell(25, 6, 'Text', 0, 0, 'L', false);
        $this->pdfHeaderFooterBrand->Output('HeaderFooterBrand.pdf', 'I');
        exit();
    }

    /**
     * @return JsonModel
     */
    public function pdfOrderAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(1)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $orderUuid = $this->params('uuid');
        $this->pdfOrder->setLang('de');
        $this->pdfOrder->setUserdataAddressRecipient(['Douglas Adams', 'Sonnenallee 42', '42000 Anhaltern', 'Milchstraße']);
        $this->pdfOrder->makeBasics();
        $this->pdfOrder->AddPage('P');
        $this->pdfOrder->Cell(25, 6, 'Text', 0, 0, 'L', false);
        $this->pdfOrder->AddPage('P');
        $this->pdfOrder->Cell(25, 6, 'Text', 0, 0, 'L', false);
        $this->pdfOrder->Output('Order.pdf', 'I');
        exit();
    }
}
