<?php

namespace Bitkorn\ShopTcpdf;

use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\ModuleManager\Feature\ControllerProviderInterface;
use Laminas\ModuleManager\Feature\ServiceProviderInterface;
use Laminas\ModuleManager\Feature\ViewHelperProviderInterface;
use Laminas\ServiceManager\Config;

class Module implements ConfigProviderInterface, ServiceProviderInterface, ControllerProviderInterface, ViewHelperProviderInterface
{
	/**
	 * Returns configuration to merge with application configuration
	 * @return array|\Traversable
	 */
	public function getConfig()
	{
		return include __DIR__ . '/../config/module.config.php';
	}

	/**
	 * Expected to return \Laminas\ServiceManager\Config object or array to seed such an object.
	 * @return array|Config
	 */
	public function getServiceConfig()
	{
		return [
			'factories' => [
			]
		];
	}

	/**
	 * Expected to return \Laminas\ServiceManager\Config object or array to seed such an object.
	 * @return array|Config
	 */
	public function getControllerConfig()
	{
		return [
			'factories' => [
			]
		];
	}

	/**
	 * Expected to return \Laminas\ServiceManager\Config object or array to seed such an object.
	 * @return array|Config
	 */
	public function getViewHelperConfig()
	{
		return [
		];
	}
}
