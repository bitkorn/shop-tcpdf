<?php


namespace Bitkorn\ShopTcpdf\Pdf;


abstract class PdfClass extends \TCPDF
{
    protected $pathImg;
    protected $pathPdf;

    protected $marginLeft = 25; // DIN 5008 = 25
    protected $marginTop = 20;
    protected $marginRight = 15;
    protected $marginBottom = 20;
    protected $marginBottomAddPage = 20;
    protected $marginBottomAddPageMulti = 10;

    const FONT_SIZE_XXS = 6;
    const FONT_SIZE_XS = 8;
    const FONT_SIZE_S = 10;
    const FONT_SIZE_M = 12;
    const FONT_SIZE_L = 14;
    const FONT_SIZE_XL = 16;
    const FONT_SIZE_XXL = 18;

    public $fontSizeInitial = self::FONT_SIZE_M;

    public $colorArrayWhite = [255, 255, 255];
    public $colorArrayBlack = [10, 10, 10];
    public $colorArrayGreyLight = [200, 200, 200];

    /**
     * @param mixed $pathImg
     */
    public function setPathImg($pathImg): void
    {
        $this->pathImg = $pathImg;
    }

    /**
     * @param mixed $pathPdf
     */
    public function setPathPdf($pathPdf): void
    {
        $this->pathPdf = $pathPdf;
    }

    public function AddPage($orientation = '', $format = '', $keepmargins = false, $tocpage = false)
    {
        parent::AddPage($orientation, $format, $keepmargins, $tocpage);

        /*
         * Falzmarke - https://de.wikipedia.org/wiki/Falzmarke
         * Form A = Briefblatt mit 27 mm hohem Briefkopf
         * 1. 87 mm
         * 2. 192 mm
         * Form B = Briefblatt mit 45 mm hohem Briefkopf
         * 1. 105 mm
         * 2. 210 mm
         */
        $this->setImageScale(1.25);
        $this->Image($this->pathImg . '/falzmarke.svg', 3, 87, 4, 0.1, 'svg', '', '', false, 300, '', false, false, 0);
        $this->Image($this->pathImg . '/falzmarke.svg', 3, 192, 4, 0.1, 'svg', '', '', false, 300, '', false, false, 0);
        /**
         * Lochmarke
         */
        $this->Image($this->pathImg . '/lochmarke.svg', 3, 148.5, 6, 0.1, 'svg', '', '', false, 300, '', false, false, 0);

    }

    protected function getContentWidth()
    {
        return $this->getPageWidth() - ($this->marginLeft + $this->marginRight);
    }
}
