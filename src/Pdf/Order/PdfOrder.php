<?php

namespace Bitkorn\ShopTcpdf\Pdf\Order;

use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\ShopTcpdf\Pdf\PdfClass;
use Bitkorn\ShopTcpdf\Pdf\PdfShopDefault;
use Laminas\Stdlib\ArrayUtils;

class PdfOrder extends PdfShopDefault
{
    protected $infoBox = [
        'po_number' => [
            'label' => 'Bestell Nr.',
            'value' => ''
        ],
    ];

    /**
     * @var XshopBasketEntity
     */
    protected $xBasketEntity;

    /**
     * @param string $orderNumber
     */
    public function setUserdataInfoBoxOrderNumber(string $orderNumber): void
    {
        $this->userdataInfoBox['po_number']['value'] = $orderNumber;
    }

    /**
     * @param XshopBasketEntity $xBasketEntity
     */
    public function setXBasketEntity(XshopBasketEntity $xBasketEntity): void
    {
        $this->xBasketEntity = $xBasketEntity;
    }

    /**
     * @param array $data order_number, order_filename, order_path_absolute
     * @return bool
     */
    public function makeDocument(array $data): bool
    {
        if (
            !isset($this->xBasketEntity)
            || empty($this->xBasketEntity->getStorageItems())
            || !$this->xBasketEntity->valuesComputed
            || !isset($data['order_number'])
            || !isset($data['order_filename'])
            || !isset($data['order_path_absolute'])
        ) {
            $this->addMessage('One of the following is empty, false or not set: xBasketEntity->StorageItems, xBasketEntity->valuesComputed,
             data[order_number, order_filename, order_path_absolute]');
            return false;
        }
        $this->userdataInfoBox = ArrayUtils::merge($this->userdataInfoBox, $this->infoBox);
        $basketItems = $this->xBasketEntity->getStorageItems();
        $this->setUserdataInfoBoxOrderNumber($data['order_number']);

        $this->SetFontSize(PdfClass::FONT_SIZE_XL);
        $this->SetFont($this->fontFamilyDefault, 'B');
        $this->AddPage('P');
        $this->Cell(25, 0, 'Bestellung Nr. ' . $data['order_number'], 0, 1, 'L', false);

        $this->SetFontSize(PdfClass::FONT_SIZE_M);
        $this->SetFont($this->fontFamilyDefault, '');
        $this->Ln();
        $this->Cell(25, 0, $this->userdataSalutationLine . ',', 0, 1, 'L', false);
        $this->Ln();
        $this->writeHTMLCell($this->getContentWidth(), 0, $this->marginLeft, $this->GetY(), $this->userdataContentStartHtml, 0, 1);
        $this->Ln();

        $this->Row6header('Anz.', 'ME', 'Art.-Nr.', 'Beschreibung', 'E-Preis €', 'Betrag €');

        foreach ($basketItems as $item) {
            $this->Row6(
                $this->numberFormatService->format($item['shop_article_amount']),
                'Stck',
                $item['shop_article_sku'],
                html_entity_decode($item['shop_article_name']) . "\r\n" .
                call_user_func($this->articleDescVariable, $item, false),
                $this->numberFormatService->format($item['shop_article_price']),
                $this->numberFormatService->format($item['shop_article_price_total']));
        }
        $this->Output($data['order_path_absolute'] . DIRECTORY_SEPARATOR . $data['order_filename'], 'F');
        return true;
    }
}
