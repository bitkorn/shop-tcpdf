<?php

namespace Bitkorn\ShopTcpdf\Pdf;

use Bitkorn\Shop\View\Helper\Article\ArticleDescVariable;
use Bitkorn\Shop\View\Helper\Article\Option\ShopArticleOptionsFromJson;
use Bitkorn\Shop\View\Helper\Article\Param2d\ArticleParam2dItemDesc;
use Bitkorn\Shop\View\Helper\Article\TypeDelivery\ArticleLengthcutOptionsDesc;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Bitkorn\Trinket\Service\LangService;

abstract class PdfShopDefault extends PdfHeaderFooterBrand
{
    /**
     * @var string[]
     */
    protected $messages = [];

    protected $userdataInfoBox = [
        'our_sign' => [
            'label' => 'Unser Zeichen',
            'value' => ''
        ],
        'our_contact' => [
            'label' => 'Unser Kontakt',
            'value' => ''
        ],
        'your_sign' => [
            'label' => 'Ihr Zeichen',
            'value' => ''
        ],
        'your_contact' => [
            'label' => 'Ihr Kontakt',
            'value' => ''
        ],
    ];

    /**
     * @var string
     */
    protected $userdataSalutationLine = '';

    /**
     * @var string
     */
    protected $userdataContentStartHtml = '';

    /**
     * @var string
     */
    protected $userdataContentEndHtml = '';

    /**
     * @var NumberFormatService
     */
    protected $numberFormatService;

    /**
     * @var ArticleDescVariable ViewHelper
     */
    protected $articleDescVariable;

    protected function addMessage(string $message): void
    {
        $this->messages[] = $message;
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * @param string $userdataOurSign
     */
    public function setUserdataInfoBoxOurSign(string $userdataOurSign): void
    {
        $this->userdataInfoBox['our_sign']['value'] = $userdataOurSign;
    }

    /**
     * @param string $userdataOurContact
     */
    public function setUserdataInfoBoxOurContact(string $userdataOurContact): void
    {
        $this->userdataInfoBox['our_contact']['value'] = $userdataOurContact;
    }

    /**
     * @param string $userdataYourSign
     */
    public function setUserdataInfoBoxYourSign(string $userdataYourSign): void
    {
        $this->userdataInfoBox['your_sign']['value'] = $userdataYourSign;
    }

    /**
     * @param string $userdataYourContact
     */
    public function setUserdataInfoBoxYourContact(string $userdataYourContact): void
    {
        $this->userdataInfoBox['your_contact']['value'] = $userdataYourContact;
    }

    /**
     * @param string $userdataSalutationLine
     */
    public function setUserdataSalutationLine(string $userdataSalutationLine): void
    {
        $this->userdataSalutationLine = $userdataSalutationLine;
    }

    /**
     * @param string $userdataContentStartHtml
     */
    public function setUserdataContentStartHtml(string $userdataContentStartHtml): void
    {
        $this->userdataContentStartHtml = $userdataContentStartHtml;
    }

    /**
     * @param string $userdataContentEndHtml
     */
    public function setUserdataContentEndHtml(string $userdataContentEndHtml): void
    {
        $this->userdataContentEndHtml = $userdataContentEndHtml;
    }

    /**
     * @param NumberFormatService $numberFormatService
     */
    public function setNumberFormatService(NumberFormatService $numberFormatService): void
    {
        $this->numberFormatService = $numberFormatService;
    }

    public function setLang(string $lang): void
    {
        if (!isset(LangService::$langLocaleMap[$lang])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() $lang (' . $lang . ') is not supported');
        }
        $this->numberFormatService->setLocale(LangService::$langLocaleMap[$lang]);
        parent::setLang($lang);
    }

    /**
     * @param ArticleDescVariable $articleDescVariable
     */
    public function setArticleDescVariable(ArticleDescVariable $articleDescVariable): void
    {
        $this->articleDescVariable = $articleDescVariable;
    }

    public function Header()
    {
        parent::Header();

        if ($this->getPage() == 1) {
            /*
             * Informationsblock
             */
            $this->Rect(125, 32, 75, 60); // x=125; y=32; w=75 are ISO 5008
            $this->SetFontSize(PdfClass::FONT_SIZE_S);
            $x = 126;
            $this->SetXY($x, 33);
            $labelLength = 0;
            foreach ($this->userdataInfoBox as $key => $arr) {
                if ($this->GetStringWidth($arr['label']) > $labelLength) {
                    $labelLength = $this->GetStringWidth($arr['label']);
                }
            }
            foreach ($this->userdataInfoBox as $key => $arr) {
                if (empty($arr['value'])) {
                    continue;
                }
                $this->SetX($x);
                $this->Cell($labelLength, 0, $arr['label'], 0, 0, 'L', false);
                $this->Cell(0, 0, ': ' . $arr['value'], 0, 1, 'L', false);
            }

            $this->SetXY($x, 92 - 5);
            $this->SetFont($this->fontFamilyDefault, 'B');
            $this->Cell(70, 0, 'Datum: ' . date('d.m.Y'), 0, 1, 'L', false);
        }
    }

    abstract public function makeDocument(array $data): bool;

    protected $row6_c1_w = 13;
    protected $row6_c2_w = 13;
    protected $row6_c3_w = 20;
    protected $row6_c4_w = 74;
    protected $row6_c5_w = 25;
    protected $row6_c6_w = 25;
    protected $row6_col5_x = 0;
    protected $row6_col5_y = 0;
    protected $row6_col1_y = 0;

    protected function Row6header(string $quantity, string $quantityUnit, string $productNo, string $productText, string $price, string $priceSum)
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_M);
        $this->SetFillColorArray($this->colorArrayBrand);
        $this->SetTextColorArray($this->colorArrayWhite);
        $this->Cell($this->row6_c1_w, 0, $quantity, 0, 0, 'L', true);
        $this->Cell($this->row6_c2_w, 0, $quantityUnit, 0, 0, 'L', true);
        $this->Cell($this->row6_c3_w, 0, $productNo, 0, 0, 'L', true);
        $this->Cell($this->row6_c4_w, 0, $productText, 0, 0, 'L', true);
        $this->row6_col5_x = $this->GetX();
        $this->Cell($this->row6_c5_w, 0, $price, 0, 0, 'R', true);
        $this->Cell($this->row6_c6_w, 0, $priceSum, 0, 1, 'R', true);
        $this->row6_col1_y = $this->GetY();
    }

    protected function Row6(string $quantity, string $quantityUnit, string $productNo, string $productText, string $price, string $priceSum)
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetY($this->row6_col1_y);
        $this->Cell($this->row6_c1_w, 0, $quantity, 0, 0, 'L', false);
        $this->Cell($this->row6_c2_w, 0, $quantityUnit, 0, 0, 'L', false);
        $this->Cell($this->row6_c3_w, 0, $productNo, 0, 0, 'L', false);
        $this->row6_col5_y = $this->GetY();
        $this->MultiCell($this->row6_c4_w, 0, $productText, 0, 'L', false, 1);
        $this->row6_col1_y = $this->GetY();
        $this->SetXY($this->row6_col5_x, $this->row6_col5_y);
        $this->Cell($this->row6_c5_w, 0, $price, 0, 0, 'R', false);
        $this->Cell($this->row6_c6_w, 0, $priceSum, 0, 1, 'R', false);
    }
}
