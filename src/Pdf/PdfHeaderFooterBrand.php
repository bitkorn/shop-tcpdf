<?php

namespace Bitkorn\ShopTcpdf\Pdf;

use Bitkorn\Images\Tools\Svg\SvgAspectRatio;
use Laminas\I18n\Translator\Translator;

class PdfHeaderFooterBrand extends PdfClass
{
    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var string
     */
    protected $lang = 'de';

    protected $protectPasswd = '';
    protected $fontFamilyDefault = 'helvetica';
    protected $footerPositionFromBottom = -25;

    /**
     * @var SvgAspectRatio
     */
    protected $svgAspectRatio;

    protected $positionTopmost = 6;
    protected $contentStart = 26;
    protected $creator = 'Aviation ERP';

    /**
     * @var array From config
     */
    protected $colorArrayBrand = [];

    /**
     * @var string From config
     */
    protected $pathLogo = '';

    /**
     * @var string From config
     */
    protected $pathBrand = '';

    /**
     * @var string From config
     */
    protected $pathLogoSimple = '';

    protected $logoWidth = 0;
    protected $logoHeight = 20; // DIN 5008 Briefkopf minus 7
    protected $brandWidth = 0;
    protected $brandHeight = 20; // DIN 5008 Briefkopf minus 7
    protected $logoSimpleWidth = 0;
    protected $logoSimpleHeight = 12; // fix

    protected $userdataAddressLineOrigin = '';
    protected $userdataFooterTextLangKey = '';
    protected $userdataFooterHtml = '';

    protected $userdataAddressRecipient01 = '';
    protected $userdataAddressRecipient02 = '';
    protected $userdataAddressRecipient03 = '';
    protected $userdataAddressRecipient04 = '';

    /**
     * @param Translator $translator
     */
    public function setTranslator(Translator $translator): void
    {
        $this->translator = $translator;
    }

    /**
     * @param string $lang
     */
    public function setLang(string $lang): void
    {
        if (isset($this->translator)) {
            $this->translator->setLocale($lang);
        }
        $this->lang = $lang;
    }

    /**
     * @param string $protectPasswd
     */
    public function setProtectPasswd(string $protectPasswd): void
    {
        $this->protectPasswd = $protectPasswd;
    }

    /**
     * @param array $colorArrayBrand
     */
    public function setColorArrayBrand(array $colorArrayBrand): void
    {
        $this->colorArrayBrand = $colorArrayBrand;
    }

    /**
     * @param string $pathLogo
     */
    public function setPathLogo(string $pathLogo): void
    {
        $this->pathLogo = $pathLogo;
    }

    /**
     * @param string $pathBrand
     */
    public function setPathBrand(string $pathBrand): void
    {
        $this->pathBrand = $pathBrand;
    }

    /**
     * @param string $pathLogoSimple
     */
    public function setPathLogoSimple(string $pathLogoSimple): void
    {
        $this->pathLogoSimple = $pathLogoSimple;
    }

    /**
     * Perhaps it is already set in the factory.
     * @param string $userdataAddressLineOrigin
     */
    public function setUserdataAddressLineOrigin(string $userdataAddressLineOrigin): void
    {
        $this->userdataAddressLineOrigin = $userdataAddressLineOrigin;
    }

    /**
     * Perhaps it is already set in the factory.
     * @param string $userdataFooterTextLangKey
     */
    public function setUserdataFooterTextLangKey(string $userdataFooterTextLangKey): void
    {
        $this->userdataFooterTextLangKey = $userdataFooterTextLangKey;
    }

    /**
     * @param string $userdataFooterHtml
     */
    public function setUserdataFooterHtml(string $userdataFooterHtml): void
    {
        $this->userdataFooterHtml = $userdataFooterHtml;
    }

    /**
     * @param array $addressRecipient [0 => line1, 1 => line2, 2 => line3, 3 => line4]
     */
    public function setUserdataAddressRecipient(array $addressRecipient): void
    {
        if (!is_array($addressRecipient) || count($addressRecipient) != 4) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() $addressLine needs exact four elements: line1, line2, line3, line4');
        }
        $this->userdataAddressRecipient01 = $addressRecipient[0];
        $this->userdataAddressRecipient02 = $addressRecipient[1];
        $this->userdataAddressRecipient03 = $addressRecipient[2];
        $this->userdataAddressRecipient04 = $addressRecipient[3];
    }

    public function makeBasics()
    {
        // compute images width with aspect ratio (keep height)
        $this->svgAspectRatio = new SvgAspectRatio();
        if (!$this->svgAspectRatio->loadSvg($this->pathImg . $this->pathLogo)) {
            throw new \RuntimeException('Unable to load SVG file: ' . $this->pathImg . $this->pathLogo);
        }
        $this->logoWidth = ($this->svgAspectRatio->getWidth() / $this->svgAspectRatio->getHeight()) * $this->logoHeight;

        if (!$this->svgAspectRatio->loadSvg($this->pathImg . $this->pathBrand)) {
            throw new \RuntimeException('Unable to load SVG file: ' . $this->pathImg . $this->pathBrand);
        }
        $this->brandWidth = ($this->svgAspectRatio->getWidth() / $this->svgAspectRatio->getHeight()) * $this->brandHeight;

        if (!$this->svgAspectRatio->loadSvg($this->pathImg . $this->pathLogoSimple)) {
            throw new \RuntimeException('Unable to load SVG file: ' . $this->pathImg . $this->pathLogoSimple);
        }
        $this->logoSimpleWidth = ($this->svgAspectRatio->getWidth() / $this->svgAspectRatio->getHeight()) * $this->logoSimpleHeight;

        $this->SetProtection(['modify'], null, $this->protectPasswd);
        // PDF basics
        $this->SetCreator($this->creator);
        $this->SetAuthor($this->author);
        $this->SetTitle($this->title);
        $this->SetSubject($this->subject);
        $this->SetMargins($this->marginLeft, $this->marginTop, $this->marginRight); // left, top, right
        $this->SetAutoPageBreak(true, $this->marginBottom + (-1 * $this->footerPositionFromBottom));

        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetFont($this->fontFamilyDefault, '');
        $this->SetFontSize($this->fontSizeInitial);
    }

    public function AddPage($orientation = '', $format = '', $keepmargins = false, $tocpage = false)
    {
        parent::AddPage($orientation, $format, $keepmargins, $tocpage);
        $this->SetY($this->contentStart);
    }

    public function Header()
    {
        if ($this->getPage() == 1) {
            $this->SetFontSize($this->fontSizeInitial);
            $this->ImageSVG($this->pathImg . $this->pathLogo, ($this->getPageWidth() / 2) - ($this->logoWidth / 2), $this->positionTopmost, null, $this->logoHeight);
            $this->ImageSVG($this->pathImg . $this->pathBrand, $this->getPageWidth() - $this->marginRight - $this->brandWidth, $this->positionTopmost, null, $this->brandHeight);

            $this->SetY($this->positionTopmost);
            $this->SetFillColorArray($this->colorArrayWhite);
            $this->SetTextColorArray($this->colorArrayBlack);
            $this->SetFont($this->fontFamilyDefault, 'B');
            $this->SetFontSize(PdfClass::FONT_SIZE_S);
            $this->Cell(100, 0, 'Please visit our new Website', 0, 0, 'L', false);
            $this->Ln();
            $this->SetFontSize(PdfClass::FONT_SIZE_M);
            $this->SetTextColorArray($this->colorArrayBrand);
            $this->Cell(100, 0, 'www.gomolzig.de', 0, 0, 'L', false, 'http://gomolzig.de');

            /*
             * Ruecksendeangabe
             * $this->Rect(20, 27, 85, 5);
             */
            $this->SetTextColorArray($this->colorArrayBlack);
            $this->SetFont($this->fontFamilyDefault, '');
            $this->SetFontSize(PdfClass::FONT_SIZE_XS);
            $this->SetY(27);
            $this->Cell(80, 5, $this->userdataAddressLineOrigin, 0, 0, 'L', false);
            /*
             * Zusatz und Vermerkzone
             * $this->Rect(20, 32, 85, 12);
             */
            /*
             * Anschriftzone
             * $this->Rect(20, 44, 85, 28);
             */
            $this->SetY(44);
            $this->SetFontSize(PdfClass::FONT_SIZE_M);
            $this->MultiCell(80, 0,
                $this->userdataAddressRecipient01
                . "\n" . $this->userdataAddressRecipient02
                . "\n" . $this->userdataAddressRecipient03
                . "\n" . $this->userdataAddressRecipient04,
                0, 'L', false, 1);

            /*
             * Infobox.Y + Infobox.height
             */
            $this->contentStart = 92;
        } else {
            $this->SetY($this->positionTopmost);
            $this->ImageSVG($this->pathImg . $this->pathLogoSimple, ($this->getPageWidth() / 2) - ($this->logoSimpleWidth / 2), $this->positionTopmost, null, $this->logoSimpleHeight);
            $this->SetY($this->positionTopmost + $this->logoSimpleHeight + 1);
            $this->Cell($this->getContentWidth(), 1, '', 'T', 1, 'C', false);

            $this->contentStart = $this->positionTopmost + $this->logoSimpleHeight + 1;
        }
    }

    public function Footer()
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_XXS);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetY($this->footerPositionFromBottom - 5);
        $this->Cell(0, 0, $this->translator->translate($this->userdataFooterTextLangKey, 'shop_tcpdf'), 0, 1, 'L', false);

        $this->writeHTMLCell($this->getContentWidth(), 0, $this->marginLeft, $this->footerPositionFromBottom, $this->userdataFooterHtml, 0, 1);

        $this->SetY($this->getPageHeight() - 6);
        $this->Cell($this->getContentWidth(), 0, $this->translator->translate('Seite', 'shop_tcpdf') . ' ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, 0, 'C');
    }
}
