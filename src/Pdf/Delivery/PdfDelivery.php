<?php

namespace Bitkorn\ShopTcpdf\Pdf\Delivery;

use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\ShopTcpdf\Pdf\PdfClass;
use Bitkorn\ShopTcpdf\Pdf\PdfShopDefault;
use Laminas\Stdlib\ArrayUtils;

class PdfDelivery extends PdfShopDefault
{
    protected $infoBox = [
        'dlvry_number' => [
            'label' => 'Lieferschein Nr.',
            'value' => ''
        ],
    ];

    /**
     * @var XshopBasketEntity
     */
    protected $xBasketEntity;

    /**
     * @param string $deliveryNumber
     */
    public function setUserdataInfoBoxDeliveryNumber(string $deliveryNumber): void
    {
        $this->userdataInfoBox['dlvry_number']['value'] = $deliveryNumber;
    }

    /**
     * @param XshopBasketEntity $xBasketEntity
     */
    public function setXBasketEntity(XshopBasketEntity $xBasketEntity): void
    {
        $this->xBasketEntity = $xBasketEntity;
    }

    /**
     * @param array $data delivery_number, delivery_filename, delivery_path_absolute
     * @return bool
     * @todo row6() nicht, stattdessen row4() ohne Preisangaben
     */
    public function makeDocument(array $data): bool
    {
        if (
            !isset($this->xBasketEntity)
            || empty($this->xBasketEntity->getStorageItems())
            || !$this->xBasketEntity->valuesComputed
            || !isset($data['delivery_number'])
            || !isset($data['delivery_filename'])
            || !isset($data['delivery_path_absolute'])
        ) {
            $this->addMessage('One of the following is empty, false or not set: xBasketEntity->StorageItems, xBasketEntity->valuesComputed,
             data[delivery_number, delivery_filename, delivery_path_absolute]');
            return false;
        }
        $this->userdataInfoBox = ArrayUtils::merge($this->userdataInfoBox, $this->infoBox);
        $basketItems = $this->xBasketEntity->getStorageItems();
        $this->setUserdataInfoBoxDeliveryNumber($data['delivery_number']);

        $this->SetFontSize(PdfClass::FONT_SIZE_XL);
        $this->SetFont($this->fontFamilyDefault, 'B');
        $this->AddPage('P');
        $this->Cell(25, 0, 'Lieferschein Nr. ' . $data['delivery_number'], 0, 1, 'L', false);

        $this->SetFontSize(PdfClass::FONT_SIZE_M);
        $this->SetFont($this->fontFamilyDefault, '');
        $this->Ln();
        $this->Cell(25, 0, $this->userdataSalutationLine . ',', 0, 1, 'L', false);
        $this->Ln();
        $this->writeHTMLCell($this->getContentWidth(), 0, $this->marginLeft, $this->GetY(), $this->userdataContentStartHtml, 0, 1);
        $this->Ln();

        $this->Row6header('Anz.', 'ME', 'Art.-Nr.', 'Beschreibung', 'E-Preis €', 'Betrag €');

        foreach ($basketItems as $item) {
            $this->Row6(
                $this->numberFormatService->format($item['shop_article_amount']),
                'Stck',
                $item['shop_article_sku'],
                $item['shop_article_name'] . "\r\n" .
                call_user_func($this->articleDescVariable, $item, false),
                $this->numberFormatService->format($item['shop_article_price']),
                $this->numberFormatService->format($item['shop_article_price_total']));
        }
        $this->Output($data['delivery_path_absolute'] . DIRECTORY_SEPARATOR . $data['delivery_filename'], 'F');
        return true;
    }
}
