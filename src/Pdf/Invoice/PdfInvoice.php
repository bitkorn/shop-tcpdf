<?php

namespace Bitkorn\ShopTcpdf\Pdf\Invoice;

use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\ShopTcpdf\Pdf\PdfClass;
use Bitkorn\ShopTcpdf\Pdf\PdfShopDefault;
use Laminas\Stdlib\ArrayUtils;

class PdfInvoice extends PdfShopDefault
{
    protected $infoBox = [
        'iv_number' => [
            'label' => 'Rechnung Nr.',
            'value' => ''
        ],
    ];

    /**
     * @var XshopBasketEntity
     */
    protected $xBasketEntity;

    /**
     * @param string $invoiceNumber
     */
    public function setUserdataInfoBoxInvoiceNumber(string $invoiceNumber): void
    {
        $this->userdataInfoBox['iv_number']['value'] = $invoiceNumber;
    }

    /**
     * @param XshopBasketEntity $xBasketEntity
     */
    public function setXBasketEntity(XshopBasketEntity $xBasketEntity): void
    {
        $this->xBasketEntity = $xBasketEntity;
    }

    /**
     * @param array $data invoice_number, invoice_filename, invoice_path_absolute
     * @return bool
     */
    public function makeDocument(array $data): bool
    {
        if (
            !isset($this->xBasketEntity)
            || empty($this->xBasketEntity->getStorageItems())
            || !$this->xBasketEntity->valuesComputed
            || !isset($data['invoice_number'])
            || !isset($data['invoice_filename'])
            || !isset($data['invoice_path_absolute'])
        ) {
            $this->addMessage('One of the following is empty, false or not set: xBasketEntity->StorageItems, xBasketEntity->valuesComputed,
             data[invoice_number, invoice_filename, invoice_path_absolute]');
            return false;
        }
        $this->userdataInfoBox = ArrayUtils::merge($this->userdataInfoBox, $this->infoBox);
        $basketItems = $this->xBasketEntity->getStorageItems();
        $this->setUserdataInfoBoxInvoiceNumber($data['invoice_number']);

        $this->SetFontSize(PdfClass::FONT_SIZE_XL);
        $this->SetFont($this->fontFamilyDefault, 'B');
        $this->AddPage('P');
        $this->Cell(25, 0, 'Rechnung Nr. ' . $data['invoice_number'], 0, 1, 'L', false);

        $this->SetFontSize(PdfClass::FONT_SIZE_M);
        $this->SetFont($this->fontFamilyDefault, '');
        $this->Ln();
        $this->Cell(25, 0, $this->userdataSalutationLine . ',', 0, 1, 'L', false);
        $this->Ln();
        $this->writeHTMLCell($this->getContentWidth(), 0, $this->marginLeft, $this->GetY(), $this->userdataContentStartHtml, 0, 1);
        $this->Ln();

        $this->Row6header('Anz.', 'ME', 'Art.-Nr.', 'Beschreibung', 'E-Preis €', 'Betrag €');

        foreach ($basketItems as $item) {
            $this->Row6(
                $this->numberFormatService->format($item['shop_article_amount']),
                'Stck',
                $item['shop_article_sku'],
                $item['shop_article_name'] . "\r\n" .
                call_user_func($this->articleDescVariable, $item, false),
                $this->numberFormatService->format($item['shop_article_price']),
                $this->numberFormatService->format($item['shop_article_price_total']));
        }
        $this->Output($data['invoice_path_absolute'] . DIRECTORY_SEPARATOR . $data['invoice_filename'], 'F');
        return true;
    }
}
