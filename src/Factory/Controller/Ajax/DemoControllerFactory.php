<?php

namespace Bitkorn\ShopTcpdf\Factory\Controller\Ajax;

use Bitkorn\ShopTcpdf\Controller\Ajax\DemoController;
use Bitkorn\ShopTcpdf\Pdf\Order\PdfOrder;
use Bitkorn\ShopTcpdf\Pdf\PdfHeaderFooterBrand;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class DemoControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new DemoController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setPdfHeaderFooterBrand($container->get(PdfHeaderFooterBrand::class));
        $controller->setPdfOrder($container->get(PdfOrder::class));
        return $controller;
    }
}
