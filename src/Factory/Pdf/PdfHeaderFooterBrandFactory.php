<?php


namespace Bitkorn\ShopTcpdf\Factory\Pdf;


use Bitkorn\ShopTcpdf\Pdf\PdfHeaderFooterBrand;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PdfHeaderFooterBrandFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $hf = new PdfHeaderFooterBrand();
        $hf->setTranslator($container->get('translator'));
        $config = $container->get('config')['bitkorn_shoptcpdf'];
        $hf->setPathImg($config['path_img']);
        $hf->setPathPdf($config['path_pdf']);
        $hf->setColorArrayBrand($config['brand_rgb']);
        $hf->setPathLogo($config['image_paths']['logo']);
        $hf->setPathBrand($config['image_paths']['brand']);
        $hf->setPathLogoSimple($config['image_paths']['logo_simple']);
        $hf->SetAuthor($config['data_author']);
        $hf->setUserdataAddressLineOrigin($config['address_line_origin']);
        $hf->setUserdataFooterTextLangKey($config['footer_text_line']);
        $hf->setUserdataFooterHtml($config['footer_html']);
        return $hf;
    }
}
