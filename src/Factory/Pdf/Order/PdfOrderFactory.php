<?php


namespace Bitkorn\ShopTcpdf\Factory\Pdf\Order;


use Bitkorn\Shop\View\Helper\Article\ArticleDescVariable;
use Bitkorn\Shop\View\Helper\Article\Option\ShopArticleOptionsFromJson;
use Bitkorn\Shop\View\Helper\Article\Param2d\ArticleParam2dItemDesc;
use Bitkorn\Shop\View\Helper\Article\TypeDelivery\ArticleLengthcutOptionsDesc;
use Bitkorn\ShopTcpdf\Pdf\Order\PdfOrder;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;

class PdfOrderFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PdfOrder|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $pdf = new PdfOrder();
        $pdf->setTranslator($container->get('translator'));
        $pdf->setNumberFormatService($container->get(NumberFormatService::class));
        $config = $container->get('config')['bitkorn_shoptcpdf'];
        $pdf->setProtectPasswd($config['protect_passwd']);
        $pdf->setPathImg($config['path_img']);
        $pdf->setPathPdf($config['path_pdf']);
        $pdf->setColorArrayBrand($config['brand_rgb']);
        $pdf->setPathLogo($config['image_paths']['logo']);
        $pdf->setPathBrand($config['image_paths']['brand']);
        $pdf->setPathLogoSimple($config['image_paths']['logo_simple']);
        $pdf->SetAuthor($config['data_author']);
        $pdf->setUserdataAddressLineOrigin($config['address_line_origin']);
        $pdf->setUserdataFooterTextLangKey($config['footer_text_line']);
        $pdf->setUserdataFooterHtml($config['footer_html']);
        /** @var HelperPluginManager $vhm */
        $vhm = $container->get('ViewHelperManager');
        $pdf->setArticleDescVariable($vhm->get('articleDescVariable'));
        return $pdf;
    }
}
