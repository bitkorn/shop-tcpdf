msgid ""
msgstr ""
"Project-Id-Version: Bitkorn ShopTcpdf\n"
"POT-Creation-Date: 2020-06-29 07:19+0100\n"
"PO-Revision-Date: 2020-06-29 07:25+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.4\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: de_DE\n"

# test
msgid "test"
msgstr "de-test"

#
msgid "Seite"
msgstr "Seite"

#
msgid "lang_key_pdf_footer_agb_line"
msgstr "Für dieses und alle weiteren Geschäfte gelten unsere AGB, die jederzeit eingesehen werden können."
